# grove_street
Look! The final project is [here](https://grove-street-cemetery-tour.herokuapp.com/)!

## Description
A Vue.js app that uses Google maps location tracking to take a user on a tour of Grove Street Cemetery. 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
